package com.randezvou.app.studyguide;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.TextView;

public class QuestionActivity extends Activity {
	
	private boolean answerDisplayed = false;
	private QA q = new QA();
	private TextView t;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.activity_question);
		getWindow().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher_ico);
		
		Bundle bundle = getIntent().getExtras();
		String procName = bundle.getString("procname");
		t = (TextView)findViewById(R.id.question);
		t.setText(procName);
		
		Typeface font = Typeface.createFromAsset(getAssets(), "handWritten.ttf");
		t.setTypeface(font, Typeface.BOLD);
		
		Questions.load(this);
		q = Questions.getRandomQuestion();
		t.setText( q.question );
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		
		viewClicked();
		return super.dispatchTouchEvent(ev);
	}
	
	public void viewClicked(){
		
		if(answerDisplayed)
			return;
		
		answerDisplayed = true;
		String ans = q.answer.trim();
		if( ans.equals("") )
			return;
		
		t.setText( q.answer );
	}
}
