package com.randezvou.app.studyguide;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
	
	public static SharedPreferences settings;
	private static boolean activated = true;
	
	public static void initialize(Context activity){
		
		settings = activity.getSharedPreferences("settings", 0);
		activated = settings.getBoolean("activated", activated);
	}
	
	private static void savePreferences(){
		
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("activated", activated);
		editor.commit();
	}
	
	public static boolean isActivated(){
		
		return activated;
	}
	
	public static void changeActivation(boolean activation){
		
		activated = activation;
		savePreferences();
	}
	
}
