package com.randezvou.app.studyguide;

import java.util.ArrayList;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class MainActivity extends SherlockActivity {
	
	private static final int ADD_QUESTION = 289;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ToggleButton tButton = (ToggleButton)findViewById(R.id.toggle_btn);
		Preferences.initialize(this);
		if( Preferences.isActivated() )
			tButton.setChecked(true);
		else
			tButton.setChecked(false);
		
		Questions.load(this);
		refreshView();
		
		if( !serviceRunning() ){
			
			final AlarmManager m = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
			final Intent i = new Intent(this, MyService.class);
			PendingIntent service = PendingIntent.getService(this, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
			
			m.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 5 * 1000, service);
		}
	}
	
	private boolean serviceRunning(){
		
		return MyService.running;
	}
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		
		menu.add(0, ADD_QUESTION, 0, "add").setIcon(R.drawable.add_new_icon).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int itemID = item.getItemId();
		switch(itemID){
		
		case ADD_QUESTION:
			Intent intent = new Intent(this, NewQuestionActivity.class);
			startActivityForResult(intent, ADD_QUESTION);
			break;
		}
		
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if(requestCode == ADD_QUESTION)
			refreshView();
	}
	
	private void refreshView(){
		
		LinearLayout question_view = (LinearLayout)findViewById(R.id.question_view);
		question_view.removeAllViews();
		
		ArrayList<QA> qs = Questions.getQuestions();
		for(int i = 0; i < qs.size(); i++)
			printQuestion(i, qs.get(i).question, qs.get(i).answer, question_view);
	}
	
	private void printQuestion(int index, String question, String answer, LinearLayout question_view){
		
		LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams lParamLL = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		lParamLL.setMargins(0, 0, 0, 10);
		
		LinearLayout ll = new LinearLayout(this);
		TextView q = new TextView(this);
		TextView a = new TextView(this);
		
		q.setText(question);
		q.setTextColor( getResources().getColor(R.color.dark_blue) );
		q.setTypeface(null, Typeface.BOLD);
		
		a.setText(answer);
		a.setTypeface(null, Typeface.ITALIC);
		
		q.setLayoutParams(lParams);
		a.setLayoutParams(lParams);
		ll.addView(q);
		ll.addView(a);
		
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				
				int index = (Integer)view.getTag();
				Questions.remove(index);
				refreshView();
			}
		});
		
		ll.setTag(index);
		ll.setBackgroundResource(R.drawable.ll_selector);
		ll.setLayoutParams(lParamLL);
		question_view.addView(ll);
	}
	
	public void onToggleClicked(View view){
		
		boolean on = ((ToggleButton) view).isChecked();
		
		if(on)
			Preferences.changeActivation(true);
		else
			Preferences.changeActivation(false);
		
	}
	
}














