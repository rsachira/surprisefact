package com.randezvou.app.studyguide;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.content.Context;

public class Questions {
	
	private static final String QUESTION_FILE_NAME = "questions.txt";
	private static final String DELIMITER = "<>";
	private static final String DELIMITER_2 = "==";
	
	private static Activity a;
	private static ArrayList<QA> questionsArray = new ArrayList<QA>();
	
	public static void load(Activity activity){
		
		questionsArray = new ArrayList<QA>();
		a = activity;
		
		try{
			
			FileInputStream fis = a.openFileInput(QUESTION_FILE_NAME);
			String data = convertStreamToString(fis);
			fis.close();
			
			populateArray(data);
			
		}catch(Exception e){
			
			e.printStackTrace();
		}
	}
	
	private static String convertStreamToString(InputStream is) throws Exception {
		
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	   
	    while ((line = reader.readLine()) != null) {
	      sb.append(line).append("\n");
	    }
	    
	    String data = sb.toString();
	    return data.trim();
	}
	
	private static void populateArray(String data){
		
		String[] qArray = data.split(DELIMITER);
		for(int i = 0; i < qArray.length; i++){
			
			String[] aArray = qArray[i].split(DELIMITER_2);
			QA q = new QA();
			q.question = aArray[0];
			q.answer = aArray[1];
			
			questionsArray.add(q);
		}
	}
	
	private static void save(){
		
		FileOutputStream outputStream;
		
		try{
				File f = new File(QUESTION_FILE_NAME);
				if( f.exists() )
					f.delete();
				
				outputStream = a.openFileOutput(QUESTION_FILE_NAME, Context.MODE_PRIVATE);
				outputStream.write( generateString().getBytes() );
				outputStream.close();
			  
		}catch (Exception e){
				
				e.printStackTrace();
		}
	}
	
	private static String generateString(){
		
		String data = "";
		
		for(int i = 0; i < questionsArray.size(); i++){
			
			if(i > 0)
				data += DELIMITER;
			
			QA q = questionsArray.get(i);
			data += q.question + DELIMITER_2 + q.answer;
		}
		
		return data;
	}
	
	public static void addQuestion(String question, String answer){
		
		QA q = new QA();
		q.question = question;
		q.answer = answer;
		
		questionsArray.add(q);
		save();
	}
	
	public static ArrayList<QA> getQuestions(){
		
		return questionsArray;
	}
	
	public static QA getRandomQuestion(){
		
		Random r = new Random();
		int i = r.nextInt( questionsArray.size() );
		
		return questionsArray.get(i);
	}
	
	public static void remove(int index){
		
		questionsArray.remove(index);
		save();
	}
	
}
