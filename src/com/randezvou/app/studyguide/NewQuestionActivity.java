package com.randezvou.app.studyguide;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class NewQuestionActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_question);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	public void addClicked(View view){
		
		EditText questionText = (EditText)findViewById(R.id.question_text);
		EditText answerText = (EditText)findViewById(R.id.answer_text);
		
		Questions.addQuestion( questionText.getText().toString(), answerText.getText().toString() );
		finish();
	}

}
