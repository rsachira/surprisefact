package com.randezvou.app.studyguide;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Service;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.IBinder;

public class MyService extends Service{
	
	public static boolean running = false;
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void onCreate() {
		
		MyService.running = true;
	}
	
	private static String previousProcessName;
	private static int count;
	private String nowPocName;
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		Preferences.initialize(this);
		if( !Preferences.isActivated() )
			return super.onStartCommand(intent, flags, startId);
		
		count++;
		String processName = getForegroundApp();
		
		if( isDifferentProcess(processName) )
			onAppChange();
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	private void onAppChange(){
		
		count = 0;
		
		Intent dialogIntent = new Intent(getBaseContext(), QuestionActivity.class);
		dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		dialogIntent.putExtra("procname", nowPocName);
		
		startActivity(dialogIntent);
	}
	
	private boolean isDifferentProcess(String processName){
		
		if(count > 410){	// If 30 minutes had passed without getting the chance to show a question, show it anyway.
			
			previousProcessName = processName;
			return true;
		}
		
		if(previousProcessName == null){
			nowPocName = "empty";
			previousProcessName = processName;
			return false;
		}
		nowPocName = previousProcessName;
		
		if( processName.equals("StudyGuide") || processName.equals(previousProcessName) )
			return false;
		
		previousProcessName = processName;
		return true;
	}
	
	private String getForegroundApp(){
		
		ActivityManager am = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
		RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);
		String foregroundTaskPackageName = foregroundTaskInfo .topActivity.getPackageName();
		PackageManager pm = getPackageManager();
		PackageInfo foregroundAppPackageInfo;
		try {
			foregroundAppPackageInfo = pm.getPackageInfo(foregroundTaskPackageName, 0);
		} catch (NameNotFoundException e) {
			return "";
		}
		
		if( isSystemPackage(foregroundAppPackageInfo) )
			previousProcessName = null;
		
		String foregroundTaskAppName = foregroundAppPackageInfo.applicationInfo.loadLabel(pm).toString();
		return foregroundTaskAppName;
	}
	
	private boolean isSystemPackage(PackageInfo pkgInfo) {
        return (pkgInfo.applicationInfo.flags & 
                ApplicationInfo.FLAG_SYSTEM) != 0;
    }


}
